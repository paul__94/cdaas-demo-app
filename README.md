# Spring Boot Demo App

Note to myself :) Keep this repository for VZ demo-app. Do not remove the repository.

This repository is used to test the application that is being deployed by https://github.com/ltutar/playbook-awx-artifactory-download-and-deploy

The project has been adjusted for demo purposes and certain files and folders have been removed.

# Build
## Maven
Validate

```bash
./mvnw clean validate
```

Compile

```bash
./mvnw clean compile
```

Test and create a report

```bash
./mvnw surefire-report:report
```

Build

```bash
./mvnw clean install spring-boot:repackage
```

Build a specific version

```bash
./mvnw versions:set -DnewVersion=1.0.0
./mvnw clean install spring-boot:repackage
```

## Running the application as jar

```bash
java -jar ./target/cdaas-demo-app-1.0.0.jar
```

## Docker: build, run & test
Build and run app image

```bash
docker build -t cdaas-demo-app .
docker run -p 8080:8080 cdaas-demo-app
```

Build and run e2e tests (app image must already be running)

```bash
docker build -t cdaas-demo-e2e .
docker run --rm --network host cdaas-demo-e2e
```

Build and run load tests (app image must already be running)

```bash
docker build -t cdaas-demo-load-test .
docker run --rm --network host cdaas-demo-load-test
```

## Run Ansible locally

Ansible deploys to an EC2 machine with an elastic IP.
This EC2 machine uses Ubuntu Server 18.04 as OS.
The pem to connect with the EC2 should be configured in the `~/.ssh/config` file for the elastic IP.
To run Ansible locally, set some environment variables accordingly and run the command.

```bash
export GITLAB_CR_USERNAME="<my Gitlab username>"
export GITLAB_CR_PASSWORD="<my Gitlab Access Token"
export IMAGE_TAG="<image tag of branch to deploy>"

ansible-playbook -i inventories/staging.yml -k site.yml -e gitlab_cr_username=$GITLAB_CR_USERNAME -e gitlab_cr_password=$GITLAB_CR_PASSWORD -e image_tag=$IMAGE_TAG
```

## Gitlab CI/CD variables

Although Gitlab CI/CD variables are a bad practice, to get everything working they have been used for now.
The following should be set:

* `EC2_PEM`: file with the pem to connect with the EC2 machine
* `GITLAB_CR_USERNAME`: the username of the Gitlab account
* `GITLAB_CR_PASSWORD`: a generated access token for Gitlab with the permissions `read_registry` and `write_registry`

# Future improvements

The following things could still be improved:

* Enable linting jobs to improve code quality, it would be best to create a build image which already include the linters so they don't have to be installed every time
* Reduce duplicated bash script and logic for building the Docker images, for example using [anchors](https://docs.gitlab.com/ee/ci/yaml/#anchors)
* Parameterize the `clean-up-images` job so the images for the specific environment will be cleaned up to reduce code duplication
* Move generalized jobs to a central place so they can be re-used in other pipelines
* Use Ansible Vault for secrets (EC2_PEM, GITLAB_CR_USERNAME, GITLAB_CR_PASSWORD), since Gitlab CI/CD variables are a bad practice
* For the staging intentory use a random port for each branch, because currently staging branches would have conflicting prots
* Add a rolling update strategy for the application, this would be more relevant if the inventory would have multiple hosts
* Pin versions during the Docker installation
* Currently the results of the integration tests (e2e, load test) aren't published to a central location

# Application endpoints

Replace localhost accordingly.

- Main application: http://localhost:8080/
- Actuator endpoint: http://localhost:8080/actuator
- Health: http://localhost:8080/actuator/health

![<img src="images/java-end-result.png" height="400">](images/java-end-result.png)

