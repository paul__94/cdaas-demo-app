FROM openjdk:8-jdk-alpine AS builder
LABEL stage=builder
WORKDIR /app
ADD pom.xml pom.xml
ADD src src
ADD mvnw mvnw
ADD .mvn .mvn
RUN cd /app && ./mvnw install spring-boot:repackage

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=builder /app/target/cdaas-demo-app-*.jar app.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]
