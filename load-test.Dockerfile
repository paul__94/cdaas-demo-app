FROM openjdk:8-jdk-alpine
WORKDIR /gatling
ADD pom.xml pom.xml
ADD mvnw mvnw
ADD .mvn .mvn
RUN ./mvnw dependency:resolve
ADD src/test/gatling src/test/gatling
ENTRYPOINT [ "./mvnw", "gatling:test" ]